﻿using Xunit;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private List<Partner> CreateTestPartner()
        {
            List<Partner> partners = new List<Partner>()
            { new Partner
                {
                    Name = "Name",
                    NumberIssuedPromoCodes = 1,
                    IsActive = true,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                                                         { new PartnerPromoCodeLimit
                                                             {
                                                                 Id = Guid.Parse("3CD15FDA-8D7A-4DFD-A3BB-99D8CA410831"),
                                                                 PartnerId = Guid.Parse("3CD15FDA-8D7A-4DFD-A3BB-99D8CA410831"),
                                                                 Partner = new Partner(),
                                                                 CreateDate = DateTime.Now,
                                                                 CancelDate = DateTime.Now,
                                                                 EndDate = DateTime.Now,
                                                                 Limit = 1
                                                             }
                                                         }
                }
            };
            return partners;
        }
        private PartnersController partnerController;

        private Mock<IRepository<Partner>> _partnersRepositoryMock = new Mock<IRepository<Partner>>();
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            partnerController = new PartnersController(_partnersRepositoryMock.Object);
        }
        [Fact]
        //проверка что метод GetPartnersAsync вернет ожидаемые результаты в случае корректных параметров
        public async void GetAllPartner_ExistingPartner_Success()
        {
            //Arrange
            List<Partner> partners = CreateTestPartner();
            var response = partners.Select(x => new PartnerResponse()
            {
                Id = x.Id,
                Name = x.Name,
                NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
                IsActive = true,
                PartnerLimits = x.PartnerLimits
                    .Select(y => new PartnerPromoCodeLimitResponse()
                    {
                        Id = y.Id,
                        PartnerId = y.PartnerId,
                        Limit = y.Limit,
                        CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                        CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                    }).ToList()
            });
            _partnersRepositoryMock.Setup(m => m.GetAllAsync()).ReturnsAsync(partners);
            //Act
            var result = await partnerController.GetPartnersAsync();
            //Assert
            var okRequestResult = Assert.IsType<ActionResult<List<PartnerResponse>>>(result);
            okRequestResult.Result.Should().BeAssignableTo<OkObjectResult>();

            var listPartnerResponse = Assert.IsAssignableFrom<IEnumerable<PartnerResponse>>(((OkObjectResult)okRequestResult.Result).Value);
            var itemPartnerResponse = listPartnerResponse.ToList().First();
            itemPartnerResponse.Name.Should().Equals(itemPartnerResponse.Name);
        }
        [Fact]
        //проверка что метод GetPartnersAsync вернет статус код 404 в случае если Partner не существует
        public async void GetAllPartner_NullPartner_ErrorStatusCode404()
        {
            //Arrange
            List<Partner> partners = new List<Partner>();
            _partnersRepositoryMock.Setup(m => m.GetAllAsync()).ReturnsAsync(partners);
            //Act
            var result = await partnerController.GetPartnersAsync();
            //Assert
            var errorRequestResult = Assert.IsType<ActionResult<List<PartnerResponse>>>(result);
            errorRequestResult.Result.Should().BeAssignableTo<NotFoundResult>();
        }
        [Fact]
        //проверка что метод GetPartnersAsync вернет статус код 400 в случае если Partner.IsActive = false
        public async void GetAllPartner_PartnerIsActiveFalse_ErrorStatusCode400()
        {
            //Arrange
            List<Partner> partners = CreateTestPartner();
            partners.First().IsActive = false;
            _partnersRepositoryMock.Setup(m => m.GetAllAsync()).ReturnsAsync(partners);
            //Act
            var result = await partnerController.GetPartnersAsync();
            //Assert
            var badRequestResult = Assert.IsType<ActionResult<List<PartnerResponse>>>(result);
            badRequestResult.Result.Should().BeAssignableTo<BadRequestResult>();
        }
        [Fact]
        //проверка что метод SetPartnerPromoCodeLimitAsync вернет ожидаемые результаты в случае корректных параметров
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_Success()
        {
            //Arrange
            Guid id = new Guid();
            Partner partner = CreateTestPartner().First();
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(partner);
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 1
            };
            //Act
            var result = await partnerController.SetPartnerPromoCodeLimitAsync(id, request);
            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            Assert.IsType<CreatedAtActionResult>(result).StatusCode.Should().Equals(201);
        }
        [Fact]
        //проверка что метод SetPartnerPromoCodeLimitAsync обнулит NumberIssuedPromoCodes
        //и добавит в список PartnerLimits элемент, в котором установит новую дату отмены лимита если партнеру выставляется лимит 
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_SetNewCancelDate()
        {
            //Arrange
            Guid id = new Guid();
            Partner partner = CreateTestPartner().First();
            partner.PartnerLimits.First().CancelDate = null;
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(partner);
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 1
            };
            //Act
            var result = await partnerController.SetPartnerPromoCodeLimitAsync(id, request);
            //Assert
            var okRequestResult = Assert.IsType<CreatedAtActionResult>(result);
            var resultList = (Partner)okRequestResult.RouteValues.Where(i => i.Key == "partner").Select(a => a.Value).First();
            resultList.NumberIssuedPromoCodes.Should().Equals(0);
            resultList.PartnerLimits.Where(x => x.CancelDate.HasValue).Should().NotBeNull();
        }
        [Fact]
        //проверка что в методе SetPartnerPromoCodeLimitAsync лимит должен быть > 0, проверка на возврат статус кода 400 при Limit<0
        public async void SetPartnerPromoCodeLimitAsync_LimitBelowZero_ErrorStatusCode400()
        {
            //Arrange
            Guid id = new Guid();
            Partner partner = CreateTestPartner().First();
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(partner);
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = -1
            };
            //Act
            var result = await partnerController.SetPartnerPromoCodeLimitAsync(id, request);
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        [Fact]
        //проверка что в методе SetPartnerPromoCodeLimitAsync однократно вызывается метод FE репозитория UpdateAsync, то есть изменения были сохранены
        public async void SetPartnerPromoCodeLimitAsync_CorrectParameters_SaveInDataBase()
        {
            //Arrange
            Partner partner = CreateTestPartner().First();
            Guid id = new Guid();
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Now,
                Limit = 1
            };
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(id)).ReturnsAsync(partner);
            //Act
            await partnerController.SetPartnerPromoCodeLimitAsync(id, request);
            //Assert
            _partnersRepositoryMock.Verify(m => m.UpdateAsync(It.IsAny<Partner>()), Times.Exactly(1));
        }


    }
}